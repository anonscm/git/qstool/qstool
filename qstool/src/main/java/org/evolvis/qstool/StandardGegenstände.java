package org.evolvis.qstool;

import java.awt.FlowLayout;
import java.awt.GridLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.BufferedReader;
import java.io.FileReader;
import java.io.IOException;

import javax.swing.DefaultListModel;
import javax.swing.JButton;
import javax.swing.JComboBox;
import javax.swing.JFrame;
import javax.swing.JList;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JTextField;

public class StandardGegenstände {
	JPanel anzeige = new JPanel( );
	
	DefaultListModel reihenfolgen = new DefaultListModel();
	
	JButton hinzufügen = new JButton("Reihenfolge hinzufügen");
	JButton entfernen = new JButton("Reihenfolge entfernen");
	JButton bearbeiten = new JButton("Reihenfolge bearbeiten");
	JButton zurück = new JButton("zurück");
	JButton ändern = new JButton("Änderung speichern");
	
	JTextField text = new JTextField(20);
	
	JPanel buttonPanel = new JPanel( );
	
	static String ENCODING = System.getProperty("user.home")
			+ System.getProperty("file.separator")+ ".qstool"
			+ System.getProperty("file.separator") + "encodings.txt";
	static String INHALT = System.getProperty("user.home")
			+ System.getProperty("file.separator")+ ".qstool"
			+ System.getProperty("file.separator") +"standardreihenfolgen.txt";
	
	public void reihenfolge(JFrame f){
		f.setVisible(true);
		
		anzeige.setLayout( new GridLayout(1, 2) );
		
		buttonPanel.setLayout( new GridLayout(4, 1));
		
		JList liste = new JList( reihenfolgen );
		
		String dateiname = INHALT;
		int zeilenAnzahl = getZeilenanzahl(dateiname);
		
		String[] einträge = new String[zeilenAnzahl];
		leseZeilen(einträge, zeilenAnzahl, dateiname);
		
		for(String s : einträge){
			reihenfolgen.addElement(s);
		}
		
		anzeige.add(new JScrollPane(liste));
		
		actions(f, einträge, liste);
		buttonPanel.add(hinzufügen);
		buttonPanel.add(entfernen);
		buttonPanel.add(bearbeiten);
		buttonPanel.add(zurück);
		JPanel kleiner = new JPanel( new FlowLayout() );
		kleiner.add(buttonPanel);
		anzeige.add(kleiner);
		
		f.add(anzeige);
		f.pack();
	}
	
	private void actions(final JFrame f, final String[] einträge, final JList liste){
		ActionListener edit = new ActionListener(){
			public void actionPerformed(ActionEvent e) {
				if(liste.getSelectedIndex() == -1)
					JOptionPane.showMessageDialog(f, "Die zu bearbeitende Reihenfolge angeben!");
				else{
					if(StandardActions.sichereDatei( liste.getSelectedValue() ) ){
						StandardActions std = new StandardActions();
						anzeige.setVisible(false);
						std.bearbeiten(f, einträge, liste.getSelectedIndex());
						anzeige.removeAll();
						}
						else{
							JOptionPane.showMessageDialog(f, "Diese Reihenfolge darf nicht gelöscht oder bearbeitet werden!");
						}
				}
				}
		};
		bearbeiten.addActionListener(edit);
		
		ActionListener delete = new ActionListener(){
			public void actionPerformed(ActionEvent e) {
				if(liste.getSelectedIndex() == -1)
					JOptionPane.showMessageDialog(f, "Die zu löschende Reihenfolge angeben!");
				else{
					if(StandardActions.sichereDatei( liste.getSelectedValue() ) ){
						StandardActions std = new StandardActions();
						anzeige.setVisible(false);
						std.entfernen(f, einträge, liste.getSelectedIndex());
						anzeige.removeAll();
					}
					else{
						JOptionPane.showMessageDialog(f, "Diese Reihenfolge darf nicht gelöscht oder bearbeitet werden!");
					}
				}
				}
		};
		entfernen.addActionListener(delete);
		
		ActionListener adden = new ActionListener(){
			public void actionPerformed(ActionEvent e) {
				StandardActions std = new StandardActions();
				anzeige.setVisible(false);
				std.hinzufügen(f, einträge);
				anzeige.removeAll();
				}
		};
		hinzufügen.addActionListener(adden);
		
		ActionListener back = new ActionListener(){
			public void actionPerformed(ActionEvent e) {
				Creation cre = new Creation();
				anzeige.setVisible(false);
				cre.show(f);
				anzeige.removeAll();
				
			}
		};
		zurück.addActionListener(back);
	}

	public static void leseZeilen(String[] einträge, int zeilenAnzahl,
			String dateiname) {
		try{
    		BufferedReader bufferedreader = new BufferedReader(new FileReader(dateiname));
    		String temp;
    		int i = 0;
    		while((temp = bufferedreader.readLine()) != null){
    			einträge[i] = temp;
    			++i;
    		}
    		bufferedreader.close();
    	}
    	catch(IOException e){
    		System.out.print("Reihenfolgeliste nicht gefunden");
    	}
	}

	public static int getZeilenanzahl(String dateiname) {
		int zeilenAnzahl = 0;
		try{
    		BufferedReader bufferedreader = new BufferedReader(new FileReader(dateiname));
    		while( bufferedreader.readLine() != null){
    			++zeilenAnzahl;
    		}
    		bufferedreader.close();
    	}
    	catch(IOException e){
    		System.out.print("Reihenfolgeliste nicht gefunden");
    	}
		return zeilenAnzahl;
	}

	public void dateiname(JFrame f) {
		f.setVisible(true);
		
		anzeige.setLayout( new FlowLayout() );
		buttonPanel.setLayout( new GridLayout(2, 1) );
		
		text.setText(getDateiname());
		
		zurück.setText("Änderung verwerfen");
		zurück.addActionListener(StandardActions.abbrechen(f, anzeige));
		
		ändern.addActionListener(StandardActions.änderungspeichern(f, anzeige, text));
		
		buttonPanel.add(zurück);
		buttonPanel.add(ändern);
		
		anzeige.add(text);
		anzeige.add(buttonPanel);
		
		f.add(anzeige);
		f.pack();
	}
	
	public static String getDateiname(){
		String rückgabe = "";
		try{
    		BufferedReader bufferedreader = new BufferedReader(new FileReader(System.getProperty("user.home")
    				+ System.getProperty("file.separator")+ ".qstool"
    				+ System.getProperty("file.separator") + "dateiname.txt"));
    		rückgabe = bufferedreader.readLine();
    		bufferedreader.close();
    	}
    	catch(IOException e){
    	}
		return rückgabe;
	}
	
	public static void fillBox(JComboBox box, String dateiname){
		
		int zeilenAnzahl = getZeilenanzahl(dateiname);
		
		String[] einträge = new String[zeilenAnzahl];
		leseZeilen(einträge, zeilenAnzahl, dateiname);
		
		for(String s : einträge)
			box.addItem(s);
	}
	
}


















