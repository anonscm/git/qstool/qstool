package org.evolvis.qstool;

import java.awt.Dimension;
import java.awt.FlowLayout;
import java.awt.GridLayout;
import java.awt.Toolkit;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.ItemEvent;
import java.awt.event.ItemListener;
import java.io.File;

import javax.swing.BoxLayout;
import javax.swing.JButton;
import javax.swing.JComboBox;
import javax.swing.JFileChooser;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JRadioButton;
import javax.swing.JTextField;
import javax.swing.border.TitledBorder;


public class DateiErstellen {
	/*Sachen für dateigröße*/
	JLabel sogroß = new JLabel("Die Datei soll...");
	JLabel großsein = new JLabel("...groß sein.");
	
	JTextField dataSize = new JTextField(10);
	
	JPanel bytAnordnung = new JPanel( new GridLayout(3, 1) );
	
	JRadioButton byt = new JRadioButton("B");
	JRadioButton kbyt = new JRadioButton("KB");
	JRadioButton mbyt = new JRadioButton("MB");
	/*Sachen für dateigröße*/
	
	
	/*Sachen für encoding*/
	JComboBox encodingBox = new JComboBox();
	/*Sachen für encoding*/
	
	
	/*Sachen für dateiformat*/
	JPanel formatAnordnung = new JPanel( new GridLayout(2, 1) );
	
	JRadioButton txt = new JRadioButton("txt");
	JRadioButton pdf = new JRadioButton("pdf");
	/*Sachen für dateiformat*/
	
	
	/*Sachen für inhalt*/
	JLabel inhaltLabel = new JLabel("Was soll in die Datei?");
	
	JPanel inhaltPanel = new JPanel( new FlowLayout() );
	
	JComboBox inhaltBox = new JComboBox();
	
	JTextField inhaltText = new JTextField(10);
	/*Sachen für inhalt*/
	
	/*Sachen für name*/
	JTextField dataName = new JTextField(StandardGegenstände.getDateiname(), 30);
	
	JPanel namenAnordnung = new JPanel( new FlowLayout() );
	
	JButton suchen = new JButton("durchsuchen");
	/*Sachen für name*/
	
	/*Sachen für leerzeichen*/
	JPanel leerzeichenAnordnung = new JPanel( new GridLayout(2, 1) );
	
	JRadioButton zulassen = new JRadioButton("ja");
	JRadioButton nichtzulassen = new JRadioButton("nein");
	/*Sachen für leerzeichen*/
	
	/*Sachen für Zeilenumbruch*/
	JPanel zeilenumbruchAnordnung = new JPanel( new FlowLayout() );
	
	JLabel nach = new JLabel("Nach ");
	JLabel einbauen = new JLabel("Zeichen Zeilenumbruch einbauen.");
	JTextField zeilen = new JTextField("", 5);
	/*Sachen für Zeilenumbruch*/
	
	/*Sachen für button*/
	JButton zurück = new JButton("zurück");
	JButton erstellen = new JButton("Datei erstellen");
	/*Sachen für button*/
	
	
	JPanel dateigröße = new JPanel( new FlowLayout() );
	JPanel encoding = new JPanel( new FlowLayout() );
	JPanel dateiformat = new JPanel( new FlowLayout() );
	JPanel inhalt = new JPanel( );
	JPanel name = new JPanel( new FlowLayout() );
	JPanel leerzeichen = new JPanel( new FlowLayout() );
	JPanel zeilenumbruch = new JPanel( new FlowLayout() );
	JPanel button = new JPanel( new FlowLayout() );
	
	JPanel anzeige = new JPanel( new GridLayout(4, 1, 5, 0) );
	JPanel anzeigeAnordnung1 = new JPanel( new GridLayout() );
	JPanel anzeigeAnordnung2 = new JPanel( new GridLayout() );
	JPanel anzeigeAnordnung3 = new JPanel( new GridLayout() );
	JPanel anzeigeAnordnung4 = new JPanel( new GridLayout() );
	
	public void show(JFrame f){
		initSuchenButton(f);
		initRadioButton();
		
		initDateigröße();
		initEncoding();
		initDateiformat();
		initInhalt();
		initName();
		initLeerzeichen();
		initZeilenumbruch();
		initButton(f);
		
		dateigröße.setBorder( new TitledBorder("Dateigröße(nur bis ca. 175MB möglich!!!)") );
		encoding.setBorder( new TitledBorder("Encoding (nur nötig bei txt-Dateien)") );
		dateiformat.setBorder( new TitledBorder("Dateiformat") );
		inhalt.setBorder( new TitledBorder("Inhalt") );
		name.setBorder( new TitledBorder("Dateiname") );
		leerzeichen.setBorder( new TitledBorder("Leerzeichen zulassen?") );
		zeilenumbruch.setBorder( new TitledBorder("Zeilenumbruch(optional)") );
		
		anzeigeAnordnung1.add(dateigröße);
		anzeigeAnordnung1.add(encoding);
		anzeigeAnordnung2.add(dateiformat);
		anzeigeAnordnung2.add(inhalt);
		anzeigeAnordnung3.add(name);
		anzeigeAnordnung3.add(leerzeichen);
		anzeigeAnordnung4.add(zeilenumbruch);
		anzeigeAnordnung4.add(button);
		
		anzeige.add(anzeigeAnordnung1);
		anzeige.add(anzeigeAnordnung2);
		anzeige.add(anzeigeAnordnung3);
		anzeige.add(anzeigeAnordnung4);
		f.add(anzeige);
		f.setSize(1038, 500);
		Dimension d = Toolkit.getDefaultToolkit().getScreenSize();
		f.setLocation( ( d.width - 1038) / 2, ( d.height - 500 ) / 2 );
	}
	
	private void initSuchenButton(final JFrame f){
		
		ActionListener seek = new ActionListener(){
			public void actionPerformed( ActionEvent e ){
				JFileChooser chooser = new JFileChooser();
			    chooser.showOpenDialog(f);
			    try{
			    	int i = 1;
			    	String temp = chooser.getSelectedFile().getPath();
			    	while(new File(chooser.getSelectedFile() + ".txt").exists() 
			    			|| new File(chooser.getSelectedFile() + ".pdf").exists() ){
			    		chooser.setSelectedFile( new File(temp + "_"+ (i + "")));
			    		++i;
			    	}
			    	
			    	dataName.setText( chooser.getSelectedFile().getPath() );
			    }catch( Exception exc ){}
			}
		};
		suchen.addActionListener(seek);
	}

	private void initDateigröße() {
		bytAnordnung.add(byt);
		bytAnordnung.add(kbyt);
		bytAnordnung.add(mbyt);
		
		dateigröße.add(sogroß);
		dateigröße.add(dataSize);
		dateigröße.add(bytAnordnung);
		dateigröße.add(großsein);
	}
	
	private void initEncoding() {
		StandardGegenstände.fillBox(encodingBox, StandardGegenstände.ENCODING);
		encoding.add(encodingBox);
	}
	
	private void initDateiformat() {
		formatAnordnung.add(txt);
		formatAnordnung.add(pdf);
		
		dateiformat.add(formatAnordnung);
	}
	
	private void initInhalt() {
		StandardGegenstände.fillBox(inhaltBox, StandardGegenstände.INHALT);
		inhaltBox.addItem("eigene Reihenfolge");
		inhaltPanel.add(inhaltLabel);
		inhaltPanel.add(inhaltBox);
		inhalt.setLayout(new BoxLayout(inhalt, BoxLayout.Y_AXIS));
		inhalt.add(inhaltPanel);
		final JPanel kleiner = new JPanel( new FlowLayout() );
		kleiner.add(inhaltText);
		inhalt.add(kleiner);
		inhaltText.setVisible(false);
		ItemListener own = new ItemListener(){
			public void itemStateChanged( ItemEvent e){
				if(inhaltBox.getSelectedItem().equals("eigene Reihenfolge")){
					inhaltText.setVisible(true);
					inhalt.doLayout();
					kleiner.doLayout();
				}
				else{
					inhaltText.setVisible(false);
				}
			}
		};
		inhaltBox.addItemListener(own);
		
	}
	
	private void initName() {
		int i = 1;
		String temp = dataName.getText();
    	while(new File(dataName.getText() + ".txt").exists() 
    			|| new File(dataName.getText() + ".pdf").exists() ){
    		dataName.setText(  temp + "_"+ (i + ""));
    		++i;
    	}
		
		namenAnordnung.add(dataName);
		namenAnordnung.add(suchen);
		
		name.add(namenAnordnung);
	}
	
	private void initLeerzeichen() {
		leerzeichenAnordnung.add(zulassen);
		leerzeichenAnordnung.add(nichtzulassen);
		
		leerzeichen.add(leerzeichenAnordnung);
	}
	
	private void initZeilenumbruch() {
		zeilenumbruchAnordnung.add(nach);
		zeilenumbruchAnordnung.add(zeilen);
		zeilenumbruchAnordnung.add(einbauen);
		
		zeilenumbruch.add(zeilenumbruchAnordnung);
	}
	
	private void initButton(final JFrame f) {
		ActionListener back = new ActionListener(){
			public void actionPerformed( ActionEvent e ){
				Creation cre = new Creation();
				anzeige.setVisible(false);
				cre.show(f);
				anzeige.removeAll();
			}
		};
		zurück.addActionListener(back);
		
		ActionListener create = new ActionListener(){
			public void actionPerformed( ActionEvent e ){
				int größe = 0;
				int format = 0;
				String enc = "";
				String art = "";
				String dateiname = dataName.getText();
				
				if(eingabekontrolle(f)){
					größe = Integer.valueOf( dataSize.getText() );
					if(kbyt.isSelected())
						größe *= 1024;
					if(mbyt.isSelected())
						größe *= (1024 * 1024);
				
				
					if(txt.isSelected()){
						format = 1;
					}
					if(pdf.isSelected()){
						format = 2;
					}
					
					enc = (String) encodingBox.getSelectedItem();
					
					art = (String) inhaltBox.getSelectedItem();
					if(art.equals("eigene Reihenfolge"))
						art = inhaltText.getText();
					if( zulassen.isSelected() ){
						art += " ";
					}else{
						String[] temp = art.split(" ");
						art = "";
						for(String s : temp)
							art += s;
					}
					
					
					if(!zeilen.getText().equals("") && txt.isSelected()){
						int newLine = Integer.valueOf( zeilen.getText() );
						AnzahlVonZeichenErstellen anz = new AnzahlVonZeichenErstellen();
						art = anz.umbruchString(art, newLine, true);
					}
					if(!zeilen.getText().equals("") && pdf.isSelected())
						art = pdfumbruch(art);
					Finish fin = new Finish();
					anzeige.setVisible(false);
					fin.schreibeInGröße(f, dateiname, größe, format, enc, art);
					anzeige.removeAll();
				}
			}

			
		};
		erstellen.addActionListener(create);
		
		button.add(zurück);
		button.add(erstellen);
	}
	
	protected String pdfumbruch(String art) {
		StringBuffer rückgabe = new StringBuffer(2 * art.length());
		for(int i = 0; i < art.length(); ++i){
			rückgabe.append(art.charAt(i));
			if( (i+1) % Integer.valueOf(zeilen.getText()) == 0 )
				rückgabe.append("\n");
		}
		return rückgabe.substring(0);
	}

	private boolean eingabekontrolle(JFrame f){
		boolean rückgabe = true;
		//Eingabe Dateigröße
		try{
			Integer.valueOf( dataSize.getText() );
			if(Integer.valueOf( dataSize.getText() ) < 950 && pdf.isSelected() 
					&& byt.isSelected() && rückgabe){
				JOptionPane.showMessageDialog(f, "Bei pdf-Dateien immer mind. 950B als Dateigröße angeben.");
				rückgabe = false;
			}
		}
		catch( Exception e){
			dataSize.setText("");
			JOptionPane.showMessageDialog(f, "Dateigröße bitte korrekt eingeben!");
			rückgabe = false;
		}
		if( !( byt.isSelected()  || kbyt.isSelected() || mbyt.isSelected() ) 
				&& rückgabe){
			JOptionPane.showMessageDialog(f, "B, KB oder MB angeben!");
			rückgabe = false;
		}
		
		if( !( pdf.isSelected() || txt.isSelected() ) && rückgabe){
			JOptionPane.showMessageDialog(f, "Dateiformat angeben!");
			rückgabe = false;
		}
		
		if(!zulassen.isSelected() && !nichtzulassen.isSelected() && rückgabe){
			JOptionPane.showMessageDialog(f, "Leerzeichen zulassen oder ablehenen angeben!");
			rückgabe = false;
		}
		
		if(inhaltBox.getSelectedItem().equals("eigene Reihenfolge") 
				&& inhaltText.getText().equals("") && rückgabe){
			JOptionPane.showMessageDialog(f, "Bei eigener Reihenfolge Text eingeben!");
			rückgabe = false;
		}
		

		if(dataName.getText().equals("") && rückgabe){
			JOptionPane.showMessageDialog(f, "Einen Dateinamen angeben!");
			rückgabe = false;
		}
		if(!zeilen.getText().equals("") && rückgabe){
			try{
				Integer.valueOf( zeilen.getText() );
			}
			catch( Exception e){
				zeilen.setText("");
				JOptionPane.showMessageDialog(f, "Zeilenumbruch bitte korrekt eingeben!");
				rückgabe = false;
			}
		}
		return rückgabe;
	}
	
	
	private void initRadioButton() {
		/*byt  kbyt mbyt*/
		ActionListener selectbyt = new ActionListener(){
			public void actionPerformed( ActionEvent e ){
				byt.setSelected(true);
				kbyt.setSelected(false);
				mbyt.setSelected(false);
			}
		};
		byt.addActionListener(selectbyt);
		
		ActionListener selectkbyt = new ActionListener(){
			public void actionPerformed( ActionEvent e ){
				byt.setSelected(false);
				kbyt.setSelected(true);
				mbyt.setSelected(false);
			}
		};
		kbyt.addActionListener(selectkbyt);
		
		ActionListener selectmbyt = new ActionListener(){
			public void actionPerformed( ActionEvent e ){
				byt.setSelected(false);
				kbyt.setSelected(false);
				mbyt.setSelected(true);
			}
		};
		mbyt.addActionListener(selectmbyt);
		/*byt  kbyt mbyt*/
		
		
		/*format*/
		ActionListener selecttxt = new ActionListener(){
			public void actionPerformed( ActionEvent e ){
				txt.setSelected(true);
				pdf.setSelected(false);
			}
		};
		txt.addActionListener(selecttxt);
		
		ActionListener selectpdf = new ActionListener(){
			public void actionPerformed( ActionEvent e ){
				txt.setSelected(false);
				pdf.setSelected(true);
			}
		};
		pdf.addActionListener(selectpdf);
		/*format*/
		
		/*Leerzeichen*/
		ActionListener selectja = new ActionListener(){
			public void actionPerformed( ActionEvent e ){
				zulassen.setSelected(true);
				nichtzulassen.setSelected(false);
			}
		};
		zulassen.addActionListener(selectja);
		
		ActionListener selectnein = new ActionListener(){
			public void actionPerformed( ActionEvent e ){
				nichtzulassen.setSelected(true);
				zulassen.setSelected(false);
			}
		};
		nichtzulassen.addActionListener(selectnein);
		/*Leerzeichen*/
	}
}


















