package org.evolvis.qstool;


import java.awt.BorderLayout;
import java.awt.Dimension;
import java.awt.FlowLayout;
import java.awt.Toolkit;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.BufferedWriter;
import java.io.FileWriter;
import java.io.IOException;

import javax.swing.BoxLayout;
import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JTextField;

public class StandardActions {
	JFrame frame = new JFrame();
	
	JPanel anzeige = new JPanel();
	
	JTextField text = new JTextField(20);
	
	JButton zurück = new JButton();
	JButton durchführen = new JButton();
	JPanel buttonPanel = new JPanel( new FlowLayout() );
	
	JLabel label = new JLabel("", JLabel.CENTER);
	
	public void hinzufügen(final JFrame f, final String[] einträge) {
		f.setVisible(false);
		erstelleFrame();
		anzeige.setLayout( new BoxLayout(anzeige, BoxLayout.Y_AXIS));
		
		durchführen.setText("Hinzufügen");
		buttonPanel.add(durchführen);
		zurück.setText("Zurück");
		buttonPanel.add(zurück);
		
		JPanel kleiner = new JPanel( new FlowLayout() );
		
		kleiner.add(text);
		
		anzeige.add(kleiner);
		anzeige.add(buttonPanel);
		
		frame.add(anzeige);
		
		ActionListener back = new ActionListener(){
			public void actionPerformed(ActionEvent e) {
				StandardGegenstände std = new StandardGegenstände();
				std.reihenfolge(f);
				frame.removeAll();
				frame.dispose();
			}
		};
		zurück.addActionListener(back);
		
		ActionListener adden = new ActionListener(){
			public void actionPerformed(ActionEvent e) {
					if(!text.getText().equals("")){
						if(!exists(text.getText())){
							String[] str = new String[einträge.length + 1];
							for(int i = 0; i < einträge.length; ++i)
								str[i] = einträge[i];
							str[einträge.length] = text.getText();
				
							überschreibe(System.getProperty("user.home")
			    				+ System.getProperty("file.separator")+ ".qstool"
			    				+ System.getProperty("file.separator") + "standardreihenfolgen.txt", str);
							StandardGegenstände std = new StandardGegenstände();
							std.reihenfolge(f);
							frame.removeAll();
							frame.dispose();
						}
						else{
							JOptionPane.showMessageDialog(frame, "Diese Reihenfolge existiert bereits!");
						}
					}
					else{
						JOptionPane.showMessageDialog(frame, "Reihenfolge eingeben");
					}
			}
		};
		durchführen.addActionListener(adden);
	}
	
	
	
	
	public void entfernen( final JFrame f, final String[] einträge, final int selectedIndex) {
		f.setVisible(false);
		erstelleFrame();
		frame.setTitle("Zeichenkette löschen");
		anzeige.setLayout(new BorderLayout(100, 10));
		
		label.setText("Soll die Reihenfolge " + einträge[selectedIndex] + " wirklich gelöscht werden?");
		
		buttonPanel.setLayout(new FlowLayout());
		zurück.setText("Nein");
		buttonPanel.add(zurück);
		durchführen.setText("Ja");
		buttonPanel.add(durchführen);
		
		anzeige.add(label, BorderLayout.NORTH);
		anzeige.add(buttonPanel, BorderLayout.SOUTH);
		
		frame.add(anzeige);
		frame.pack();
		
		ActionListener delete = new ActionListener(){
			public void actionPerformed(ActionEvent e) {
					String[] str = new String[einträge.length - 1];
					for(int i = 0, l = 0; i < einträge.length; ++i, ++l){
						if(i != selectedIndex)
							str[l] = einträge[i];
						else
							l--;
					}
					überschreibe(System.getProperty("user.home")
		    				+ System.getProperty("file.separator")+ ".qstool"
		    				+ System.getProperty("file.separator") + "standardreihenfolgen.txt", str);
					StandardGegenstände std = new StandardGegenstände();
					std.reihenfolge(f);
					frame.removeAll();
					frame.dispose();
			}
		};
		durchführen.addActionListener(delete);
		
		ActionListener cancel = new ActionListener(){
			public void actionPerformed(ActionEvent e) {
					StandardGegenstände std = new StandardGegenstände();
					std.reihenfolge(f);
					frame.removeAll();
					frame.dispose();
			}
		};
		zurück.addActionListener(cancel);
	}
	
	
	
	public void bearbeiten(final JFrame f, final String[] einträge, final int selectedIndex) {
		f.setVisible(false);
		erstelleFrame();
		frame.setTitle("Zeichenkette bearbeiten");
		anzeige.setLayout(new BorderLayout(100, 5));
		
		text.setText(einträge[selectedIndex]);
		anzeige.add(text, BorderLayout.NORTH);
		
		zurück.setText("Zurück");
		buttonPanel.add(zurück);
		durchführen.setText("Änderung speichern");
		buttonPanel.add(durchführen);
		anzeige.add(buttonPanel, BorderLayout.SOUTH);
		
		frame.add(anzeige);
		
		ActionListener save = new ActionListener(){
			public void actionPerformed(ActionEvent e) {
					if(text.getText().equals(""))
						JOptionPane.showMessageDialog(frame, "Bitte Text eingeben");
					else{
						if(!exists(text.getText())){
							einträge[selectedIndex] = text.getText();
							überschreibe(System.getProperty("user.home")
			    				+ System.getProperty("file.separator")+ ".qstool"
			    				+ System.getProperty("file.separator") +"standardreihenfolgen.txt", einträge);
							StandardGegenstände std = new StandardGegenstände();
							std.reihenfolge(f);
							frame.removeAll();
							frame.dispose();
						}
						else{
							JOptionPane.showMessageDialog(frame, "Diese Reihenfolge existiert bereits!");
						}
					}
			}
		};
		durchführen.addActionListener(save);
		
		ActionListener cancel = new ActionListener(){
			public void actionPerformed(ActionEvent e) {
					StandardGegenstände std = new StandardGegenstände();
					std.reihenfolge(f);
					frame.removeAll();
					frame.dispose();
			}
		};
		zurück.addActionListener(cancel);
	}
	
	
	private boolean exists(String text) {
		int anzahl = StandardGegenstände.getZeilenanzahl(StandardGegenstände.INHALT);
		String[] reihenfolgen = new String[anzahl];
		StandardGegenstände.leseZeilen(reihenfolgen, anzahl, StandardGegenstände.INHALT);
		
		for(String str : reihenfolgen){
			if(str.equals(text))
				return true;
		}
		
		return false;
	}
	
	
	
	private void überschreibe(String dateiname, String[] einträge) {
		try{
    		BufferedWriter bufferedwriter = new BufferedWriter(new FileWriter(dateiname));
    		for(int i = 0; i < einträge.length; ++i){
    			bufferedwriter.write(einträge[i]);
    			if(i != einträge.length - 1)
    				bufferedwriter.write("\n");
    		}
    		bufferedwriter.close();
    	}
    	catch(IOException e){
    		System.out.print("Reihenfolgeliste konnte nicht geändert werden");
    	}
	}


	private void erstelleFrame() {
		frame.setTitle("Zeichenkette hinzufügen");
		frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		frame.setSize(300, 150);
		Dimension d = Toolkit.getDefaultToolkit().getScreenSize();
		frame.setLocation( ( d.width - 300) / 2, ( d.height - 200 ) / 2 );
		frame.setVisible(true);
	}




	public static ActionListener abbrechen(final JFrame f, final JPanel panel) {
		ActionListener  al = new ActionListener(){
			public void actionPerformed(ActionEvent e) {
				Creation cre = new Creation();
				panel.setVisible(false);
				cre.show(f);
				panel.removeAll();
			}
		};
		return al;
	}




	public static ActionListener änderungspeichern(final JFrame f, final JPanel panel, final JTextField text) {
		ActionListener  al = new ActionListener(){
			public void actionPerformed(ActionEvent e) {
				try{
					BufferedWriter bufferedwriter = new BufferedWriter(new FileWriter(System.getProperty("user.home")
		    				+ System.getProperty("file.separator")+ ".qstool"
		    				+ System.getProperty("file.separator") +"dateiname.txt"));
		    		bufferedwriter.write(text.getText());
		    		bufferedwriter.close();
		    		JOptionPane.showMessageDialog(f, "Änderung gespeichert");
		    		Creation cre = new Creation();
					panel.setVisible(false);
					cre.show(f);
					panel.removeAll();
				}
				catch( IOException exc){
					JOptionPane.showMessageDialog(f, "Änderung konnte nicht gespeichert werden");
				}
			}
		};
		return al;
	}




	public static boolean sichereDatei(Object selectedValue) {
		String sichereReihenfolgen = System.getProperty("user.home")
				+ System.getProperty("file.separator")+ ".qstool"
				+ System.getProperty("file.separator") + "sicherungsdatei.txt";
		int zeilenAnzahl = StandardGegenstände.getZeilenanzahl(sichereReihenfolgen);
		String[] sichereDat = new String[zeilenAnzahl];
		StandardGegenstände.leseZeilen(sichereDat, zeilenAnzahl, sichereReihenfolgen);
		for(String str : sichereDat){
			if( ( (String) selectedValue).equals(str))
				return false;
		}
		return true;
	}
	
}






