package org.evolvis.qstool;

import java.awt.Dimension;
import java.awt.FlowLayout;
import java.awt.GridLayout;
import java.awt.Toolkit;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.ItemEvent;
import java.awt.event.ItemListener;
import javax.swing.BoxLayout;
import javax.swing.JButton;
import javax.swing.JComboBox;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JRadioButton;
import javax.swing.JTextField;
import javax.swing.border.TitledBorder;

public class AnzahlVonZeichenErstellen {
	/*Sachen für anzahl*/
	JLabel anzahlLabel1 = new JLabel("Ich brauche ");
	JLabel anzahlLabel2 = new JLabel(" Zeichen.");
	
	JTextField anzahlText = new JTextField(10);
	/*Sachen für anzahl*/
	
	
	/*Sachen für inhalt*/
	JPanel inhaltPanel = new JPanel( new FlowLayout() );
	
	JLabel inhaltLabel = new JLabel("Was für Zeichen sollen es sein? ");
	
	JTextField inhaltText = new JTextField(10);
	
	JComboBox inhaltBox = new JComboBox();
	/*Sachen für inhalt*/
	
	
	/*Sachen für leerzeichen*/
	JRadioButton zulassen = new JRadioButton("ja");
	JRadioButton nichtzulassen = new JRadioButton("nein");
	/*Sachen für leerzeichen*/
	
	
	/*Sachen für Zeilenumbruch*/
	JLabel zeilenumbruchLabel1 = new JLabel("Nach ");
	JLabel zeilenumbruchLabel2 = new JLabel("Zeichen Zeilenumbruch einbauen.");
	JTextField zeilenText = new JTextField("", 5);
	/*Sachen für Zeilenumbruch*/
	
	
	/*Sachen für button*/
	JButton abbr = new JButton("abbrechen");
	JButton erstellen = new JButton("Zeichen erstellen");
	/*Sachen für button*/
	
	
	JPanel anzeige = new JPanel( new GridLayout(3, 1) );
	
	JPanel anzeigeAnordnung1 = new JPanel( new GridLayout(1, 2) );
	JPanel anzeigeAnordnung2 = new JPanel( new GridLayout(1, 2) );
	
	JPanel anzahl = new JPanel( new FlowLayout() );
	JPanel inhalt = new JPanel( /*new BorderLayout()*/ );
	JPanel leerzeichen = new JPanel( new FlowLayout() );
	JPanel zeilenumbruch = new JPanel( new FlowLayout() );
	JPanel button = new JPanel( new FlowLayout() );
	
	
	public void show(JFrame f, String keepZeichen){
		initRadiobutton();
		
		initEigenschaften();
		initBu(f, keepZeichen);
		
		anzahl.setBorder( new TitledBorder("Anzahl der Zeichen"));
		inhalt.setBorder( new TitledBorder("Reihenfolge"));
		leerzeichen.setBorder( new TitledBorder("Leerzeichen zulassen?"));
		zeilenumbruch.setBorder( new TitledBorder("Zeilenumbruch?(optional)"));
		button.add(abbr);
		button.add(erstellen);
		
		anzeigeAnordnung1.add(anzahl);
		anzeigeAnordnung1.add(inhalt);
		anzeigeAnordnung2.add(leerzeichen);
		anzeigeAnordnung2.add(zeilenumbruch);
		
		anzeige.add(anzeigeAnordnung1);
		anzeige.add(anzeigeAnordnung2);
		anzeige.add(button);
		
		f.add(anzeige);
		f.setSize(1038, 500);
		Dimension d = Toolkit.getDefaultToolkit().getScreenSize();
		f.setLocation( ( d.width - 1038) / 2, ( d.height - 500 ) / 2 );
	}
	
	private void initEigenschaften(){
		anzahl.add(anzahlLabel1);
		anzahl.add(anzahlText);
		anzahl.add(anzahlLabel2);
		
		
		StandardGegenstände.fillBox(inhaltBox, StandardGegenstände.INHALT);
		inhaltBox.addItem("eigene Reihenfolge");
		
		inhaltPanel.add(inhaltLabel);
		inhaltPanel.add(inhaltBox);
		inhalt.setLayout(new BoxLayout(inhalt, BoxLayout.Y_AXIS));
		inhalt.add(inhaltPanel);
		final JPanel kleiner = new JPanel( new FlowLayout() );
		kleiner.add(inhaltText);
		inhalt.add(kleiner);
		inhaltText.setVisible(false);
		ItemListener own = new ItemListener(){
			public void itemStateChanged( ItemEvent e){
				if(inhaltBox.getSelectedItem().equals("eigene Reihenfolge")){
					inhaltText.setVisible(true);
					inhalt.doLayout();
					kleiner.doLayout();
				}
				else{
					inhaltText.setVisible(false);
				}
			}
		};
		inhaltBox.addItemListener(own);
		
		
		
		leerzeichen.add(zulassen);
		leerzeichen.add(nichtzulassen);
		
		
		zeilenumbruch.add(zeilenumbruchLabel1);
		zeilenumbruch.add(zeilenText);
		zeilenumbruch.add(zeilenumbruchLabel2);
	}
	
	
	private void initBu(final JFrame f, final String keepZeichen){
		ActionListener cancel = new ActionListener(){
			public void actionPerformed(ActionEvent e) {
				Creation cre = new Creation();
				anzeige.setVisible(false);
				cre.show(f);
				anzeige.removeAll();
			}
		};
		abbr.addActionListener(cancel);
		
		ActionListener create = new ActionListener(){
			public void actionPerformed(ActionEvent e) {
				if(eingabekontrolle(f)){
					String zeichen = erstelleZeichen(f);
					if(!zeichen.equals("")){
						zeichen = keepZeichen + zeichen;
						Produkt prod = new Produkt();
						anzeige.setVisible(false);
						prod.show(f, zeichen);
						anzeige.removeAll();
					}
				}
			}
		};
		erstellen.addActionListener(create);
	}
	
	private String erstelleZeichen(final JFrame f){
		String rückgabe = "";
		String art = "";
		int newLine = 0;
		if(!zeilenText.getText().equals(""))
			newLine = Integer.valueOf( zeilenText.getText() );
		int anzahlZeichen = Integer.valueOf( anzahlText.getText() );
		
		art = (String) inhaltBox.getSelectedItem();
		if(art.equals("eigene Reihenfolge"))
			art = inhaltText.getText();
		if( zulassen.isSelected() ){
			art += " ";
		}else{
			String[] temp = art.split(" ");
			art = "";
			for(String s : temp)
				art += s;
		}
		
		
		if(art.equals("")){
			JOptionPane.showMessageDialog(f, "Wenn du nur " +
					"Leerzeichen haben willst, musst du sie auch zulassen!");
		}
		else{
			if(!zeilenText.getText().equals("")){
				art = umbruchString( art, newLine, true);
			}
			rückgabe = umbruchString(art, anzahlZeichen, false);
		}
		return rückgabe;
	}
	
	public String umbruchString( String art, int newLine, boolean mitnewLine){
		StringBuffer buffer = new StringBuffer( newLine + 1 );
		int index = 0, länge = art.length();
		
		while(index < newLine - länge){
			buffer.append(art);
			//rückgabe += inhalt;
			index += länge;
		}
		for(int i = 0; i < newLine - index; ++i)
			buffer.append( art.charAt(i) );
			//rückgabe += inhalt.charAt(i);
		if(mitnewLine){
			buffer.append('\n');
			return buffer.substring(0, newLine + 1 );
		}
		else
			return buffer.substring(0, newLine);
	}
	
	
	private boolean eingabekontrolle(JFrame f){
		boolean rückgabe = true;
		
		try{
			Integer.valueOf( anzahlText.getText() );
		}catch( NumberFormatException e ){
			JOptionPane.showMessageDialog(f, "Anzahl der Zeichen korrekt angeben!");
			rückgabe = false;
		}
		
		if(rückgabe)
			if( !zulassen.isSelected() && !nichtzulassen.isSelected()){
				JOptionPane.showMessageDialog(f, "Leerzeichen zulassen oder ablehenen angeben!");
				rückgabe = false;
			}
		
		if(rückgabe && !zeilenText.getText().equals(""))
			try{
				Integer.valueOf( zeilenText.getText() );
			}catch( Exception e ){
				JOptionPane.showMessageDialog(f, "Zeilenumbruch korrekt eingeben!");
				rückgabe = false;
			}
		
		if(inhaltBox.getSelectedItem().equals("eigene Reihenfolge") 
				&& inhaltText.getText().equals("") && rückgabe){
			JOptionPane.showMessageDialog(f, "Bei eigener Reihenfolge Text eingeben!");
			rückgabe = false;
		}
		
		return rückgabe;
	}
	
	
	private void initRadiobutton(){
		ActionListener selectja = new ActionListener(){
			public void actionPerformed( ActionEvent e ){
				zulassen.setSelected(true);
				nichtzulassen.setSelected(false);
			}
		};
		zulassen.addActionListener(selectja);
		
		ActionListener selectnein = new ActionListener(){
			public void actionPerformed( ActionEvent e ){
				nichtzulassen.setSelected(true);
				zulassen.setSelected(false);
			}
		};
		nichtzulassen.addActionListener(selectnein);
	}
}













