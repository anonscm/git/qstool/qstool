package org.evolvis.qstool;

import java.awt.Dimension;
import java.awt.FlowLayout;
import java.awt.GridLayout;
import java.awt.Toolkit;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.AbstractAction;
import javax.swing.Action;
import javax.swing.BoxLayout;
import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JMenu;
import javax.swing.JMenuBar;
import javax.swing.JPanel;

public class Creation {
	
	
	public void losgehts(){
		/*Frame erstellen*/
		JFrame f = new JFrame("Zeichen erstellen");
		f.setDefaultCloseOperation( JFrame.EXIT_ON_CLOSE );
		f.setSize(300, 200);
		Dimension d = Toolkit.getDefaultToolkit().getScreenSize();
		f.setLocation( ( d.width - 300) / 2, ( d.height - 200 ) / 2 );
		f.setVisible(true);
		show(f);
	}
	
	JMenuBar menuleiste = new JMenuBar();
	JPanel menuPanel = new JPanel( new GridLayout() );
	
	JLabel text = new JLabel( "Willst du...", JLabel.CENTER );
	JPanel textPanel = new JPanel( new FlowLayout());
	
	JButton zurGröße = new JButton("...Dateien in einer bestimmten Größe erstellen?");
	JButton zurAnzahl = new JButton("<html>...Zeichen in einer gewissen Anzahl erstellen?" +
			"<p/>(Speichern der Zeichen in einer Datei ist hier auch möglich)");
	JPanel buttonPanel = new JPanel( new GridLayout(2, 1) );
	
	JPanel anzeige = new JPanel();
	
	
	public void show(JFrame f){
		initBu(f);
		initmenu(f);
		anzeige.setLayout( new BoxLayout(anzeige, BoxLayout.Y_AXIS ));
		menuPanel.add(menuleiste);
		anzeige.add(menuPanel);
		textPanel.add(text);
		anzeige.add(textPanel);
		buttonPanel.add(zurAnzahl);
		buttonPanel.add(zurGröße);
		anzeige.add(buttonPanel);
		f.add(anzeige);
		f.pack();
		Dimension d = Toolkit.getDefaultToolkit().getScreenSize();
		f.setLocation( ( d.width - 407) / 2, ( d.height - 187 ) / 2 );
	}
	
	
	

	private void initmenu(final JFrame f) {
		JMenu features = new JMenu( "Menü" );
		
		Action changeReihenfolge = new AbstractAction( "Standardreihenfolgen ändern" ){
			public void actionPerformed( ActionEvent e){
				StandardGegenstände sta = new StandardGegenstände();
				anzeige.setVisible(false);
				sta.reihenfolge(f);
				anzeige.removeAll();
			}
		};
		features.add(changeReihenfolge);
		
		Action changeDateiname = new AbstractAction( "Standarddateiname ändern" ){
			public void actionPerformed( ActionEvent e){
				StandardGegenstände sta = new StandardGegenstände();
				anzeige.setVisible(false);
				sta.dateiname(f);
				anzeige.removeAll();
			}
		};
		features.add(changeDateiname);
		
		Action exit = new AbstractAction( "beenden" ){
			public void actionPerformed( ActionEvent e){
				System.exit(0);
			}
		};
		features.add(exit);
		
		menuleiste.add(features);
	}




	private void initBu(final JFrame f) {
		ActionListener zuZeichen = new ActionListener(){
			public void actionPerformed( ActionEvent e ){
				AnzahlVonZeichenErstellen anz = new AnzahlVonZeichenErstellen();
				anzeige.setVisible(false);
				anz.show(f, "");
				anzeige.removeAll();
			}
		};
		zurAnzahl.addActionListener(zuZeichen);
		
		ActionListener zuGröße = new ActionListener(){
			public void actionPerformed( ActionEvent e ){
				DateiErstellen dat = new DateiErstellen();
				anzeige.setVisible(false);
				dat.show(f);
				anzeige.removeAll();
			}
		};
		zurGröße.addActionListener(zuGröße);
		
	}
	
	
	
}











