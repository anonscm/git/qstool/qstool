package org.evolvis.qstool;

import java.awt.Dimension;
import java.awt.GridLayout;
import java.awt.Toolkit;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JPanel;


public class NichtSpeichern {
	JPanel anzeige = new JPanel( new GridLayout(4, 1, 30, 5));
	
	JLabel label = new JLabel("Möchtest du...", JLabel.CENTER);
	
	JButton zurück = new JButton("...zurück?");
	JButton neueErstellen = new JButton("...neue Zeichen erstellen?");
	JButton exit = new JButton("...das Progamm beenden?");
	
	public void show(JFrame f, String keepZeichen){
		init(f, keepZeichen);
		f.setSize(300, 200);
		Dimension d = Toolkit.getDefaultToolkit().getScreenSize();
		f.setLocation( ( d.width - 300) / 2, ( d.height - 200 ) / 2 );
		f.add(anzeige);
	}
	
	private void init(JFrame f, String keepZeichen){
		initBu(f, keepZeichen);
		anzeige.add(label);
		anzeige.add(zurück);
		anzeige.add(neueErstellen);
		anzeige.add(exit);
	}
	
	private void initBu(final JFrame f, final String keepZeichen){
		ActionListener back = new ActionListener(){
			public void actionPerformed( ActionEvent e ){
				Produkt prod = new Produkt();
				anzeige.setVisible(false);
				prod.show(f, keepZeichen);
				anzeige.removeAll();
			}
		};
		zurück.addActionListener(back);
		
		ActionListener end = new ActionListener(){
			public void actionPerformed( ActionEvent e ){
				System.exit(0);
			}
		};
		exit.addActionListener(end);
		
		ActionListener neue = new ActionListener(){
			public void actionPerformed( ActionEvent e ){
				Creation a = new Creation();
				anzeige.setVisible(false);
				a.show(f);
				anzeige.removeAll();
			}
		};
		neueErstellen.addActionListener(neue);
		
	}
	
	
	
}






































