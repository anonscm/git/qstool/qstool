package org.evolvis.qstool;

import java.io.BufferedReader;
import java.io.FileReader;
import java.io.IOException;


public class EncodingAuswahl {
	public String[] auswahlerstellen(){
		int zeilenAnzahl = 0;
		try{
    		BufferedReader encodingreader = new BufferedReader(new FileReader( System.getProperty("user.home")
    				+ System.getProperty("file.separator")+ ".qstool"
    				+ System.getProperty("file.separator") +"encodings.txt"));
    		while( encodingreader.readLine() != null){
    			++zeilenAnzahl;
    		}
    		encodingreader.close();
    	}
    	catch(IOException e){
    		System.out.print("Encodingliste nicht gefunden");
    	}
		
		
		String[] rückgabe = new String[zeilenAnzahl];
		
    	try{
    		BufferedReader encodingreader = new BufferedReader(new FileReader(System.getProperty("user.home")
    				+ System.getProperty("file.separator")+ ".qstool"
    				+ System.getProperty("file.separator") +"encodings.txt" ));
    		String temp;
    		int i = 0;
    		while((temp = encodingreader.readLine()) != null){
    			rückgabe[i] = temp;
    			++i;
    		}
    		encodingreader.close();
    	}
    	catch(IOException e){
    		System.out.print("Encodingliste nicht gefunden");
    	}
		
    	return rückgabe;
	}
}



















