package org.evolvis.qstool;

import java.awt.Dimension;
import java.awt.FlowLayout;
import java.awt.GridLayout;
import java.awt.Toolkit;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.BufferedWriter;
import java.io.FileOutputStream;
import java.io.OutputStreamWriter;
import java.io.UnsupportedEncodingException;

import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JPanel;

import com.lowagie.text.Document;
import com.lowagie.text.Paragraph;
import com.lowagie.text.pdf.PdfWriter;


/**
 * 
 * @author Niclas Zimmermann, tarent GmbH
 *
 */
public class Finish {
	JLabel erfolgreich = new JLabel("Die Datei wurde erfolgreich erstellt.", JLabel.CENTER);
	
	JButton zurAuswahl = new JButton("Mehr Zeichen erstellen.");
	JButton beenden = new JButton("Beenden");
	JPanel buAnordnung = new JPanel( new FlowLayout() );
	
	JPanel anzeige = new JPanel( new GridLayout(2, 1) );
	
	
	
	private void erfolgreich(JFrame f){
		initerfolgreich(f);
		f.add(anzeige);
	}

	private void initerfolgreich(JFrame f) {
		initbuerf(f);
		anzeige.add(erfolgreich);
		anzeige.add(buAnordnung);
	}

	private void initbuerf(final JFrame f) {
		ActionListener more = new ActionListener(){
			public void actionPerformed( ActionEvent e ){
				Creation cre = new Creation();
				anzeige.setVisible(false);
				cre.show(f);
				anzeige.removeAll();
			}
		};
		zurAuswahl.addActionListener(more);
		
		ActionListener exit = new ActionListener(){
			public void actionPerformed( ActionEvent e ){
				System.exit(0);
			}
		};
		beenden.addActionListener(exit);
		
		buAnordnung.add(zurAuswahl);
		buAnordnung.add(beenden);
	}
	
	
	
	
	JLabel fehlgeschlagen = new JLabel("Datei konnte nicht erstellt werden!", JLabel.CENTER);
	
	JButton nochmalversuchen = new JButton("Nochmal probieren");
	JButton abbrechen = new JButton("Abbrechen");
	JPanel button = new JPanel( new FlowLayout() );
	
	private void fehlgeschlagen(JFrame f, String encoding, String zeichen, String dateiname, int dateiformat, int aufrufVon) {
		initfehl(f, encoding, zeichen, dateiname, dateiformat, aufrufVon);
		f.add(anzeige);
	}
	
	
	private void initfehl(JFrame f, String encoding, String zeichen, String dateiname, int dateiformat, int aufrufVon) {
		initbufehl(f, encoding, zeichen, dateiname, dateiformat, aufrufVon);
		button.add(nochmalversuchen);
		button.add(abbrechen);
		anzeige.add(fehlgeschlagen);
		anzeige.add(button);
	}
	
	private void initbufehl(final JFrame f, final String encoding, 
			final String zeichen, final String dateiname, 
				final int dateiformat, final int aufrufVon) {
		ActionListener cancel = new ActionListener(){
			public void actionPerformed( ActionEvent e ){
				if(aufrufVon == 1){
					Produkt p = new Produkt();
					anzeige.setVisible(false);
					p.show(f, zeichen);
					anzeige.removeAll();
				}
				if(aufrufVon == 2){
					Creation cre = new Creation();
					anzeige.setVisible(false);
					cre.show(f);
					anzeige.removeAll();
				}
			}
		};
		abbrechen.addActionListener(cancel);
		
		ActionListener tryagain = new ActionListener(){
			public void actionPerformed( ActionEvent e ){
				if(aufrufVon == 1){
					Speichern spe = new Speichern();
					anzeige.setVisible(false);
					spe.show(f, zeichen);
					anzeige.removeAll();
				}
				if(aufrufVon == 2){
					DateiErstellen dat = new DateiErstellen();
					anzeige.setVisible(false);
					dat.show(f);
					anzeige.removeAll();
				}
			}
		};
		nochmalversuchen.addActionListener(tryagain);
		
	}
	
	
	
	
	
	
	
	
	
	public void schreibe(JFrame f, String encoding, String zeichen, String dateiname, int dateiformat, int aufrufVon ) {
		f.setSize(375, 165);
		Dimension d = Toolkit.getDefaultToolkit().getScreenSize();
		f.setLocation( ( d.width - 375) / 2, ( d.height - 165 ) / 2 );
		if(dateiformat == 1){
			dateiname += ".txt";
    		try{
    			BufferedWriter out = new BufferedWriter( 
    					new OutputStreamWriter( 
    							new FileOutputStream(dateiname), encoding ) );
    			out.write(zeichen);
    			out.close();
    			erfolgreich(f);
    		}
    		catch( Exception e ){
    			fehlgeschlagen(f, encoding, zeichen, dateiname, dateiformat, aufrufVon);
    		}
		}
		if(dateiformat == 2){
			dateiname += ".pdf";
			try{
				Document document = new Document();
				PdfWriter.getInstance(document, new FileOutputStream(dateiname));
				document.open();
				document.add(new Paragraph(zeichen));
				document.close();
				erfolgreich(f);
			}
			catch( Exception e ){
				fehlgeschlagen(f, encoding, zeichen, dateiname, dateiformat, aufrufVon);
			}
		}
	}
	
	
	
	public void zeilenumbruch(JFrame f, String dateiname, int dateigröße,
			int dateiformat, String encoding, String inhalt, int zeilenumbruch){
		
	}
	
	
	
	public void schreibeInGröße(JFrame f, String dateiname, int dateigröße,
			int dateiformat, String encoding, String inhalt){
		f.setSize(375, 165);
		Dimension d = Toolkit.getDefaultToolkit().getScreenSize();
		f.setLocation( ( d.width - 375) / 2, ( d.height - 165 ) / 2 );
		if(dateiformat == 1){
			String zeichen = erzeugeZeichen(dateigröße, encoding, inhalt);
			schreibe(f, encoding, zeichen, dateiname, dateiformat, 2 );
		}
		if(dateiformat == 2){
			dateiname += ".pdf";
			dateigröße -= 923;
			pdfSchreiben(f, dateiname, dateigröße 
					, inhalt);
		}	
	}
	
	private void pdfSchreiben(JFrame f, String dateiname, int zeichenInTitel 
			, String inhalt){
		String encoding = "8859_1";
		Document document = new Document();
		try {
			PdfWriter.getInstance(document,
					new FileOutputStream(dateiname));
			String zeichen = erzeugeZeichen(zeichenInTitel, encoding, "123");
			document.addTitle("Teste Dateigöße" + zeichen);
			document.open();
			document.add(new Paragraph(inhalt));
			
			erfolgreich(f);
		} catch ( Exception e) {
			fehlgeschlagen(f, encoding, "", dateiname, 2, 2);
		}
		document.close();
	}
	
	
	

	private String erzeugeZeichen(int dateigröße, String encoding, String inhalt) {
		String rückgabe = "";
		int länge = 0;
		try{
			länge = inhalt.getBytes(encoding).length;
			
			int encodingByteMarkers = encodingByteMarkers(encoding);
			
			dateigröße -= encodingByteMarkers;
			länge -= encodingByteMarkers;
			
			int index = 0;
			StringBuffer buffer = new StringBuffer(dateigröße);
			while(index < dateigröße - länge){
				buffer.append(inhalt);
				index += länge;
			}
			
			String[] str = new String[inhalt.length()];
			for(int i = 0; i < inhalt.length(); ++i){
				str[i] = inhalt.charAt(i) + ""; 
			}
			
			for(int i = 0; i < dateigröße - index; ){
				buffer.append(str[i%str.length]);
				i += (str[i%str.length].getBytes( encoding ).length - encodingByteMarkers);
			}
			rückgabe = buffer.substring(0);
			return rückgabe;
			
		}catch( UnsupportedEncodingException e ){
			e.printStackTrace();
			return null;
		}
	}
	private int encodingByteMarkers(String enc){
		int rückgabe = 0;
		if(enc.equals("UTF-16")) rückgabe = 2;
		if(enc.equals("x-UTF-16LE-BOM")) rückgabe = 2;
		if(enc.equals("X-UTF-32BE-BOM")) rückgabe = 4;
		if(enc.equals("X-UTF-32LE-BOM")) rückgabe = 4;
		return rückgabe;
	}
}













