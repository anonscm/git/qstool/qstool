package org.evolvis.qstool;

import java.awt.Dimension;
import java.awt.FlowLayout;
import java.awt.GridLayout;
import java.awt.Toolkit;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.File;

import javax.swing.JButton;
import javax.swing.JComboBox;
import javax.swing.JFileChooser;
import javax.swing.JFrame;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JRadioButton;
import javax.swing.JScrollPane;
import javax.swing.JTextArea;
import javax.swing.JTextField;
import javax.swing.border.TitledBorder;


public class Speichern {
	/*Sachen für encoding*/
	JComboBox encodingBox = new JComboBox();
	JTextField testmitBox = new JTextField(10);
	/*Sachen für encoding*/
	
	
	/*Sachen für dateiformat*/
	JPanel formatAnordnung = new JPanel( new GridLayout(2, 1) );
	
	JRadioButton txt = new JRadioButton("txt");
	JRadioButton pdf = new JRadioButton("pdf");
	/*Sachen für dateiformat*/
	
	
	/*Sachen für name*/
	JTextField dataName = new JTextField(StandardGegenstände.getDateiname(), 30);
	
	JPanel namenAnordnung = new JPanel( new FlowLayout() );
	
	JButton suchen = new JButton("durchsuchen");
	/*Sachen für name*/
	
	
	
	/*Sachen für button*/
	JPanel buttonAnordnung = new JPanel( new FlowLayout() );
	
	JButton zurück = new JButton("zurück");
	JButton erstellen = new JButton("Datei erstellen");
	/*Sachen für button*/
	
	JTextArea alleZeichen = new JTextArea();
	
	JPanel encoding = new JPanel( new FlowLayout() );
	JPanel dateiformat = new JPanel( new FlowLayout() );
	JPanel dateiname = new JPanel( new FlowLayout() );
	JPanel button = new JPanel( new FlowLayout() );
	
	JPanel anzeige = new JPanel( new GridLayout(3, 1) );
	JPanel anzeigeAnordnung1 = new JPanel( new GridLayout() );
	JPanel anzeigeAnordnung2 = new JPanel( new GridLayout() );
	

	public void show(JFrame f, String zeichen) {
		initButton(f, zeichen);
		initRadioButton();
		initEigenschaften();
		
		dateiformat.setBorder( new TitledBorder("Dateiformat"));
		encoding.setBorder(new TitledBorder("Encoding (nur bei txt möglich)"));
		dateiname.setBorder( new TitledBorder("Dateiname"));
		
		buttonAnordnung.add(zurück);
		buttonAnordnung.add(erstellen);
		
		anzeigeAnordnung1.add(dateiformat);
		anzeigeAnordnung1.add(encoding);
		anzeigeAnordnung2.add(dateiname);
		anzeigeAnordnung2.add(buttonAnordnung);
		
		anzeige.add(anzeigeAnordnung1);
		anzeige.add(anzeigeAnordnung2);
		alleZeichen.setText(zeichen);
		anzeige.add( new JScrollPane( alleZeichen ) );
		
		f.add(anzeige);
		f.setSize(1038, 500);
		Dimension d = Toolkit.getDefaultToolkit().getScreenSize();
		f.setLocation( ( d.width - 1038) / 2, ( d.height - 500 ) / 2 );
	}
	
	private void initButton( final JFrame f, final String zeichen){
		ActionListener seek = new ActionListener(){
			public void actionPerformed( ActionEvent e ){
				JFileChooser chooser = new JFileChooser();
			    chooser.showOpenDialog(f);
			    try{
			    	int i = 1;
			    	String temp = chooser.getSelectedFile().getPath();
			    	while(new File(chooser.getSelectedFile() + ".txt").exists() 
			    			|| new File(chooser.getSelectedFile() + ".pdf").exists() ){
			    		chooser.setSelectedFile( new File(temp + "_"+ (i + "")));
			    		++i;
			    	}
			    	dataName.setText( chooser.getSelectedFile().getPath() );
			    }catch( Exception exc ){}
			}
		};
		suchen.addActionListener(seek);
		
		ActionListener back = new ActionListener(){
			public void actionPerformed( ActionEvent e ){
				Produkt prod = new Produkt();
				anzeige.setVisible(false);
				prod.show(f, zeichen);
				anzeige.removeAll();
			}
		};
		zurück.addActionListener(back);
		
		ActionListener create = new ActionListener(){
			public void actionPerformed( ActionEvent e ){
				if(eingabekontrolle(f))
					abspeichern(f, zeichen);
			}
		};
		erstellen.addActionListener(create);
	}
	
	protected void abspeichern(final JFrame f, final String zeichen) {
		String enc = (String) encodingBox.getSelectedItem();
		
		String name = dataName.getText();
		
		int format = 2;
		if(txt.isSelected())format = 1;
		
		Finish fin = new Finish();
		anzeige.setVisible(false);
		fin.schreibe(f, enc, zeichen, name, format, 1 );
		anzeige.removeAll();
	}

	protected boolean eingabekontrolle(JFrame f) {
		boolean rückgabe = true;
		if( !( pdf.isSelected() || txt.isSelected() ) && rückgabe){
			JOptionPane.showMessageDialog(f, "Dateiformat angeben!");
			rückgabe = false;
		}
		
		if(rückgabe && dataName.getText().equals("") ){
			JOptionPane.showMessageDialog(f, "Dateinamen angeben!");
			rückgabe = false;
		}
		return rückgabe;
	}

	private void initEigenschaften(){
		formatAnordnung.add(txt);
		formatAnordnung.add(pdf);
		dateiformat.add(formatAnordnung);
		
		StandardGegenstände.fillBox(encodingBox, StandardGegenstände.ENCODING);
		encoding.add(encodingBox);
		
		int i = 1;
		String temp = dataName.getText();
    	while(new File(dataName.getText() + ".txt").exists() 
    			|| new File(dataName.getText() + ".pdf").exists() ){
    		dataName.setText(  temp + "_"+ (i + ""));
    		++i;
    	}
		
		namenAnordnung.add(dataName);
		namenAnordnung.add(suchen);
		dateiname.add(namenAnordnung);
	}
	
	private void initRadioButton(){
		ActionListener selecttxt = new ActionListener(){
			public void actionPerformed( ActionEvent e ){
				txt.setSelected(true);
				pdf.setSelected(false);
			}
		};
		txt.addActionListener(selecttxt);
		
		ActionListener selectpdf = new ActionListener(){
			public void actionPerformed( ActionEvent e ){
				txt.setSelected(false);
				pdf.setSelected(true);
			}
		};
		pdf.addActionListener(selectpdf);
	}
}



