package org.evolvis.qstool;

import java.awt.Dimension;
import java.awt.FlowLayout;
import java.awt.GridLayout;
import java.awt.Toolkit;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.BoxLayout;
import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JTextArea;

public class Produkt {
	JLabel frage = new JLabel("Möchtest du die Zeichen in eine Datei schreiben?", JLabel.CENTER);
	JLabel ergebnis = new JLabel("Deine Zeichen sind:", JLabel.CENTER);
	
	JButton ja = new JButton("ja");
	JButton nein = new JButton("nein");
	JPanel jain = new JPanel( new FlowLayout() );
	
	JButton hinzufügen = new JButton("Mehr Zeichen hinzufügen");
	JButton andere = new JButton("Andere Zeichen erzeugen");
	JPanel hinand = new JPanel( new FlowLayout() );
	
	JTextArea alleZeichen = new JTextArea(21, 138);

	
	JPanel anzeigeAnordnung = new JPanel( new GridLayout(4, 1) );
	JPanel anzeige = new JPanel( );
	
	
	public void show(JFrame f, String keepZeichen){
		anzeige.setLayout( new BoxLayout( anzeige , BoxLayout.Y_AXIS) );
		alleZeichen.setText( keepZeichen );
		init(f);
		f.add(anzeige);
		f.setSize(1038, 500);
		Dimension d = Toolkit.getDefaultToolkit().getScreenSize();
		f.setLocation( ( d.width - 1038) / 2, ( d.height - 500 ) / 2 );
	}
	
	private void init(JFrame f){
		initPanel(f);
		anzeigeAnordnung.add(hinand);
		anzeigeAnordnung.add(frage);
		anzeigeAnordnung.add(jain);
		anzeigeAnordnung.add(ergebnis);
		anzeige.add(anzeigeAnordnung);
		anzeige.add( new JScrollPane(alleZeichen) );
		
	}

	private void initPanel(JFrame f) {
		initButton(f);
		hinand.add(hinzufügen);
		hinand.add(andere);
		jain.add(ja);
		jain.add(nein);
	}

	private void initButton(final JFrame f) {
		ActionListener more = new ActionListener(){
			public void actionPerformed( ActionEvent e ){
				AnzahlVonZeichenErstellen anz = new AnzahlVonZeichenErstellen();
				anzeige.setVisible(false);
				anz.show(f, alleZeichen.getText());
				anzeige.removeAll();
			}
		};
		hinzufügen.addActionListener(more);
		
		ActionListener neue = new ActionListener(){
			public void actionPerformed( ActionEvent e ){
				Creation cre = new Creation();
				anzeige.setVisible(false);
				cre.show(f);
				anzeige.removeAll();
			}
		};
		andere.addActionListener(neue);
		
		ActionListener no = new ActionListener(){
			public void actionPerformed( ActionEvent e ){
				NichtSpeichern nic = new NichtSpeichern();
				anzeige.setVisible(false);
				nic.show(f, alleZeichen.getText());
				anzeige.removeAll();
			}
		};
		nein.addActionListener(no);
		
		ActionListener yes = new ActionListener(){
			public void actionPerformed( ActionEvent e ){
				Speichern spe = new Speichern();
				anzeige.setVisible(false);
				spe.show(f, alleZeichen.getText());
				anzeige.removeAll();
			}
		};
		ja.addActionListener(yes);
	}
	
	
	
}



























