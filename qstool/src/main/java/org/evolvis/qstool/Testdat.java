package org.evolvis.qstool;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;

import javax.swing.BoxLayout;
import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JPanel;



public class Testdat {
	private static String ENC = System.getProperty("user.home") + "\\hidden_qstool_res\\" + "encodings.txt"; 
	private static String SAV = System.getProperty("user.home") + "\\hidden_qstool_res\\" + "sicherungsdatei.txt";
	
	
	public static String USRSHARE(){
		String separator = System.getProperty("file.separator");
		
		/*
		 * 
		 * ".qstool" sollte eigentlich versteckt sein!!!
		 * 
		 * 
		 * 
		 * 
		 */
		String dirName = System.getProperty("user.home") + separator + ".qstool";
		String fileName = System.getProperty("user.home") + separator 
													+ ".qstool" + separator;
		
		if( ! (new File(fileName + "dateiname.txt")).exists() ||
				! (new File(fileName + "encodings.txt")).exists() ||
				! (new File(fileName + "sicherungsdatei.txt")).exists() ||
				! (new File(fileName + "standardreihenfolgen.txt")).exists()) {
			try{
				File file = new File(dirName);
				file.mkdir();
				String copyString = "";
				String temp = "";
				
				copyString = System.getProperty("user.home") + separator + "TestZeichen";
				BufferedWriter nameWriter = new BufferedWriter( new FileWriter( fileName + "dateiname.txt"));
				nameWriter.write(copyString);
				nameWriter.close();
				copyString = "";
				
				BufferedReader encReader = new BufferedReader(new FileReader(ENC));
				while( (temp = encReader.readLine()) != null ){
					copyString += temp;
					copyString += "\n";
				}
				encReader.close();
				
				BufferedWriter encWriter = new BufferedWriter( new FileWriter( fileName + "encodings.txt"));
				encWriter.write(copyString);
				encWriter.close();
				copyString = "";
				
				BufferedReader savReader = new BufferedReader(new FileReader(SAV));
				while( (temp = savReader.readLine()) != null ){
					copyString += temp;
					copyString += "\n";
				}
				savReader.close();
				
				BufferedWriter reihenfolgeWriter = new BufferedWriter( new FileWriter( fileName + "standardreihenfolgen.txt"));
				reihenfolgeWriter.write(copyString);
				reihenfolgeWriter.close();
				
				BufferedWriter savWriter = new BufferedWriter( new FileWriter( fileName + "sicherungsdatei.txt"));
				savWriter.write(copyString);
				savWriter.close();
				copyString = "";
			}
			catch( IOException e){
				JFrame nostartFrame = new JFrame();
				nostartFrame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
				JPanel anzeige = new JPanel();
				anzeige.setLayout( new BoxLayout(anzeige, BoxLayout.Y_AXIS));
				JLabel fehlerLabel = new JLabel("Dateien, die zum starten des Programms nötig sind, sind kaputt");
				JButton fehlerButton = new JButton("ok");
				anzeige.add(fehlerLabel);
				anzeige.add(fehlerButton);
				
				nostartFrame.add(anzeige);
				
				ActionListener al = new ActionListener(){
					public void actionPerformed(ActionEvent e) {
						System.exit(0);
					}
				};
				fehlerButton.addActionListener(al);
			}
			
		}
		
		return null;
	}
	
	public static void main(String[] args){
		Creation c = new Creation();
		c.losgehts();
		USRSHARE();
	}
}